#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

__title__ ="Canal +";
__author__="Jonas Fourquier" #alias SnouF de mythtv-fr.org
__version__="0.2"
__usage__ ='''
(Option Help)
> %(file)s -h
Usage: %(file)s [option][<search>]
Version: %(version)s Author: %(author)s

For details on the MythTV Netvision plugin see the wiki page at:
http://www.mythtv.org/wiki/MythNetvision

Options:
  -h, --help            show this help message and exit
  -v, --version         Display grabber name and supported options
  -c:, --configure      Configure this grabber
  -S:, --search=        Search <search> for videos
  -T, --treeview        Display a Tree View of a sites videos
'''%{'file':__file__, 'version':__version__,'author':__author__}


import sys, os
from time import time
from getopt import getopt
from xml.dom import minidom
from urllib import urlopen, quote
from datetime import datetime
from ConfigParser import RawConfigParser

import pytz
tzParis = pytz.timezone ("CET")

import logging
logging.basicConfig(level=logging.DEBUG,format='%(levelname)s line %(lineno)d - %(message)s')
logger = logging.getLogger(__title__)

reload(sys)
sys.setdefaultencoding('utf-8')


# INIT
# Url des sources xml
urlCatalogThematiques = 'http://service.canal-plus.com/video/rest/initPlayer/cplus/'
urlCatalogMeas = 'http://service.canal-plus.com/video/rest/getMEAs/cplus/%s'
urlSearch = 'http://service.canal-plus.com/video/rest/search/cplus/%s'
urlCatalogVideos = 'http://service.canal-plus.com/video/rest/getVideos/cplus/%s'
# Nombre de vidéo démandée lors de la recherche de metadonnée
nbVideosRequet = 500
# Url du site
baseUrlSite = 'http://www.canalplus.fr/'
# Url du lecteur
#   - %(id)s : id de la vidéo
#   - %(url)s : url du fichier flv
baseUrlPlayer = 'http://player.canalplus.fr/#/%(id)s'
# Débit videos
debitVideo = 'HLS' #'BAS_DEBIT', 'HAUT_DEBIT', 'HD', 'MOBILE, 'HDS', 'HLS'
# Fichier de configuration
configFile = os.environ['HOME']+'/.mythtv/mythnetvision-canalplus.cfg'
# Lecteur externe devant être utilisé ('' pour le lecteur interne)
player = None
# Argument du lecteur (voir http://www.mythtv.org/wiki/MythNetvision_Grabber_Script_Format, '' pour aucun)
playerargs = None
# "Téléchargeur" ('' pour le téléchargeur par défaut)
download = None
# Argument du "téléchargeur" (voir http://www.mythtv.org/wiki/MythNetvision_Grabber_Script_Format, '' pour aucun)
downloadargs = None
# Description de channel
channel = {
        'title' : __title__,
        'link' : baseUrlSite,
        'description' : 'Les programmes du groupe Canal+'
    }


def MinidomNode_getFirstChildByTagName(self,tag) :
    '''
        Retourne le 1er enfant directe qui a pour tag "tag". Si aucun
        retourne None"
    '''
    for node in self.childNodes :
        if node.tagName == tag :
            return node
    return None
setattr(minidom.Node,'getFirstChildByTagName',MinidomNode_getFirstChildByTagName)


def MinidomNode_getChildByTagName(self,tag) :
    '''
        Retourne l'ensemble des enfants directes qui ont pour tag "tag".
        Si aucun une liste vide"
    '''
    nodes = []
    for node in self.childNodes :
        if node.tagName == tag :
            nodes.append(node)
    return nodes
setattr(minidom.Node,'getChildsByTagName',MinidomNode_getChildByTagName)


def MinidomNode_getChildByAttribute(self,attrname, attrvalue) :
    '''
        Retourne le 1er enfant directe qui a pour l'attribut "attrname" la
        valeur "attrvalue". Si aucun retourne False.
    '''
    for node in self.childNodes :
        if node.hasAttribute(attrname) :
            if node.getAttribute(attrname) == attrvalue :
                return node
    return False
setattr(minidom.Node,'getChildByAttribute',MinidomNode_getChildByAttribute)


def MinidomNode_getChildByChild(self,tagName, tagValue) :
    '''
        Retourne le 1er enfant directe qui a pour comme tag "tagName" la
        valeur "tagValue", Si aucun retourne False.
    '''
    for node in self.childNodes :
        if node.tagName == tagName :
            try :
                if node.firstChild.data == tagValue :
                    return node
            except :
                pass
    return False
setattr(minidom.Node,'getChildByChild',MinidomNode_getChildByChild)


def MinidomDocument_createNode(self,tag,text='',attributes={}) :
    '''
        Retourne un noeud avec pour tag "tag", comme text "text" ou comme
        attributs le dico "attributes" ({attrName: attrValue})
    '''
    node = self.createElement(tag)
    if text :
        node.appendChild(self.createTextNode(text))
    for (name,value) in attributes.items() :
        node.setAttribute(name,value)
    return node
setattr(minidom.Document,'createNode',MinidomDocument_createNode)


def MinidomDocument_convertVideo2itemNode(self,videoInputNode) :
    '''
        Converti la page de item du site de canal en item Mythnetvision
    '''
    try :
        itemOutputNode = self.createNode('item')

        #différent noeud enfant
        mediaInputNode = videoInputNode.getFirstChildByTagName('MEDIA')

        #Dico video pour plus de souplesse dans le champs l'utilisant
        videoDict = {
                'id': videoInputNode.getElementsByTagName("ID")[0].firstChild.data,
                'url': mediaInputNode.getElementsByTagName(debitVideo)[0].firstChild.data
            }

        #Le titre (et sous titre)
        title = videoInputNode.getElementsByTagName('TITRE')[0].firstChild.data
        try :   sous_title = videoInputNode.getElementsByTagName('SOUS_TITRE')[0].firstChild.data
        except: pass
        else :  title += ' - ' + sous_title
        itemOutputNode.appendChild(self.createNode('title',title))

        try :
            itemOutputNode.appendChild(self.createNode('author',videoInputNode.getElementsByTagName('AUTEUR')[0].firstChild.data))
        except :
            logger.info('Pas d\'auteur pour %s'%title)

        puDate = datetime.strptime(
                videoInputNode.getElementsByTagName('DATE')[0].firstChild.data + ' ' +videoInputNode.getElementsByTagName('HEURE')[0].firstChild.data,
                '%d/%m/%Y %H:%M:%S'
            ).replace (tzinfo = tzParis)
        itemOutputNode.appendChild(self.createNode(
                'pubDate',
                puDate.strftime('%a, %d %b %Y %H:%M:%S %z')
            ))

        try :
            itemOutputNode.appendChild(self.createNode('description',videoInputNode.getElementsByTagName('DESCRIPTION')[0].firstChild.data))
        except :
            logger.info('Pas de description pour %s'%title)

        itemOutputNode.appendChild(self.createNode('link',baseUrlPlayer%videoDict))

        mediaOutputNode = itemOutputNode.appendChild(self.createNode('media:group'))
        try :
            mediaOutputNode.appendChild(self.createNode(
                    tag='media:thumbnail',
                    attributes={
                            'url': mediaInputNode.getElementsByTagName('GRAND')[0].firstChild.data
                }))
        except :
            logger.info('Pas de miniature pour %s'%title)
        mediaOutputNode.appendChild(self.createNode(
                tag='media:content',
                attributes={
                        'url':videoDict['url']
            }))

        try :
            itemOutputNode.appendChild(self.createNode('ratting',videoInputNode.getAttribute('MOYENNE')))
        except :
            logger.info('Pas de note pour %s'%title)

        if player :
            itemOutputNode.appendChild(self.createNode('player',player))
        if playerargs :
            itemOutputNode.appendChild(self.createNode('playerargs',playerargs))
        if download :
            itemOutputNode.appendChild(self.createNode('download',download))
        if downloadargs :
            itemOutputNode.appendChild(self.createNode('downloadargs',downloadargs))

        return itemOutputNode
    except :
        return None
setattr(minidom.Document,'convertVideo2itemNode',MinidomDocument_convertVideo2itemNode)




def domVersion() :
    '''
        Retourne le noeux racine permettant a mythtv de reconnaitre le nom,
        et les fonction de ce grabbeur
    '''
    domOutput = minidom.Document()
    rootNode = domOutput.appendChild(domOutput.createNode('grabber'))
    rootNode.appendChild(domOutput.createNode('name',__title__))
    rootNode.appendChild(domOutput.createNode('command',__file__))
    rootNode.appendChild(domOutput.createNode('author',__author__))
    rootNode.appendChild(domOutput.createNode('thumbnail','canalplus.png'))
    rootNode.appendChild(domOutput.createNode('type','video'))
    rootNode.appendChild(domOutput.createNode('description',channel['description']))
    rootNode.appendChild(domOutput.createNode('version',__version__))
    rootNode.appendChild(domOutput.createNode('search','true'))
    rootNode.appendChild(domOutput.createNode('tree','true'))
    return rootNode


def domTreeview() :
    '''
        Retourne un domDocument pour la vue arbre de MythNetVision
    '''

    config = RawConfigParser()
    config.read(configFile)
    if not config.has_section('programs') or not config.has_option('programs','subscribed') :
        '''Le grabbeur n'a pas été configurer, retourne une erreur'''
        sys.stderr.write('Erreur, le grabbeur Canal+ n\'est pas configuré. Lancez "%s --configure"'%__file__)
        sys.exit(1)
    subscribedPrograms = config.get('programs','subscribed').split(',')

    # dom xml de sortie
    domOutput = minidom.Document()
    nodeRss = domOutput.appendChild(domOutput.createNode(
            tag='rss',
            attributes={
                    'version':'2.0',
                    'xmlns:itunes':'http://www.itunes.com/dtds/podcast-1.0.dtd',
                    'xmlns:content':'http://purl.org/rss/1.0/modules/content/',
                    'xmlns:cnettv':'http://cnettv.com/mrss/',
                    'xmlns:creativeCommons':'http://cnettv.com/mrss/',
                    'xmlns:media':'http://search.yahoo.com/mrss/',
                    'xmlns:atom':'http://www.w3.org/2005/Atom',
                    'xmlns:amp':'http://www.adobe.com/amp/1.0',
                    'xmlns:dc':'http://purl.org/dc/elements/1.1/',
                    'xmlns:mythtv':'http://www.mythtv.org/wiki/MythNetvision_Grabber_Script_Format'
                }))

    # node channel
    channelNode = nodeRss.appendChild(domOutput.createNode('channel'))
    channelNode.appendChild(domOutput.createNode('title',__title__))
    channelNode.appendChild(domOutput.createNode('link',channel['link']))
    channelNode.appendChild(domOutput.createNode('description',channel['description']))

    #Recup videoID :
    videosIDs = []

    #Recup des MEAids
    logger.debug('Récupération des programID sur %s'%urlCatalogThematiques)
    domInputThematiques = minidom.parse(urlopen(urlCatalogThematiques))
    for selectionInputNode in domInputThematiques.getElementsByTagName('SELECTION') :
        programID = selectionInputNode.getFirstChildByTagName('ID').firstChild.data
        programName = selectionInputNode.getFirstChildByTagName('NOM').firstChild.data
        #Recup VideosID
        if programID in subscribedPrograms : #DEBUG
            logger.info('Récupération des VideosID avec des programes "%s" sur %s'%(programName,urlCatalogThematiques))
            domInputMeasDoc = minidom.parse(urlopen(urlCatalogMeas%programID))
            measInputNode = domInputMeasDoc.getElementsByTagName('MEA')
            logger.debug('  - %i videos'%len(measInputNode))
            for meaInputNode in measInputNode :
                videosIDs.append(meaInputNode.getFirstChildByTagName('ID').firstChild.data)
        else :
            logger.info('Les programes de "%s" sont ignorés'%programName)


    #recup des videos
    nbVideos = len(videosIDs)
    logger.info('Récupération des %i videos sur %s'%(nbVideos, urlCatalogVideos))
    iVideo = 0
    while iVideo < nbVideos :
        jVideo = iVideo+nbVideosRequet
        if jVideo > nbVideos :
            jVideo = nbVideos
        logger.debug('Récupération des videos %i à %i'%(iVideo, jVideo))
        domInputVideos = minidom.parse(urlopen(urlCatalogVideos%','.join(videosIDs[iVideo:jVideo])))
        if len(domInputVideos.getElementsByTagName('VIDEO')) != jVideo-iVideo :
            logger.warning('%i vidéos ont été retournées alors que %i ont été demandées'%(len(domInputVideos.getElementsByTagName('VIDEO')),jVideo-iVideo))
        iVideo = jVideo

        for videoInputNode in domInputVideos.getElementsByTagName('VIDEO') :
            id = videoInputNode.getElementsByTagName('ID')[0].firstChild.data

            try :
                itemOutputNode = domOutput.convertVideo2itemNode(videoInputNode)

                # Classement dans les différant directory (les crée si il n'existe pas
                nodeInputRubriquage = videoInputNode.getFirstChildByTagName('RUBRIQUAGE')
                #univers
                univers = nodeInputRubriquage.getFirstChildByTagName('UNIVERS').firstChild.data
                universNode = channelNode.getChildByAttribute('name',univers)
                if not universNode :
                    '''Cette univers n'existe pas, le crée'''
                    #Canal ne propose pas d'image pour les catégories, on prend celle du 1er item qu'on croise
                    universNode = channelNode.appendChild(domOutput.createNode(
                            tag = 'directory',
                            attributes = {
                                    'name':univers,
                                    'thumbnail':itemOutputNode.getElementsByTagName('media:thumbnail')[0].getAttribute('url')
                                }
                        ))
                #rubrique
                rubrique = nodeInputRubriquage.getFirstChildByTagName('RUBRIQUE').firstChild.data
                rubriqueNode = universNode.getChildByAttribute('name',rubrique)
                if not rubriqueNode :
                    '''Cette rubrique n'existe pas, la crée'''
                    rubriqueNode = universNode.appendChild(domOutput.createNode(
                            tag = 'directory',
                            attributes = {
                                    'name':rubrique,
                                    'thumbnail':itemOutputNode.getElementsByTagName('media:thumbnail')[0].getAttribute('url')
                                }
                        ))
                #catégorie
                categorie = nodeInputRubriquage.getFirstChildByTagName('CATEGORIE').firstChild.data
                categorieNode = rubriqueNode.getChildByAttribute('name',categorie)
                if not categorieNode :
                    '''Cette categorie n'existe pas, la créer'''
                    categorieNode = rubriqueNode.appendChild(domOutput.createNode(
                            tag = 'directory',
                            attributes = {
                                    'name':categorie,
                                    'thumbnail':itemOutputNode.getElementsByTagName('media:thumbnail')[0].getAttribute('url')}
                        ))

            except Exception, e:
                '''Erreur quelque part, on saut l'élément'''
                logger.warning('id:%s error:%s'%(id,e))
            else :
                '''On insére le node item'''
                if itemOutputNode :
                    alreadyExist = False
                    for itemDoublonNode in categorieNode.getElementsByTagName('item') :
                        '''des doublons exists sur le site de canal, on passe l'éléments si c'est un doublons'''
                        if (itemOutputNode.getElementsByTagName('media:content')[0].getAttribute('url') == itemDoublonNode.getElementsByTagName('media:content')[0].getAttribute('url'))\
                        and (itemOutputNode.getElementsByTagName('title')[0].firstChild.data == itemDoublonNode.getElementsByTagName('title')[0].firstChild.data) :
                            logger.debug('%s (id %s) est un doublons'%(itemOutputNode.getElementsByTagName('title'),id))
                            alreadyExist = True
                            break
                    if not alreadyExist :
                        '''N'existe pas encore, on l'ajoute'''
                        categorieNode.appendChild(itemOutputNode)

    return domOutput


def domSearch (text) :
    '''
        Retourne un domDocument pour la recherche de MythNetVision
    '''
    domOutput = minidom.Document()
    nodeRss = domOutput.appendChild(domOutput.createNode(
            tag='rss',
            attributes={
                    'version':'2.0',
                    'xmlns:itunes':'http://www.itunes.com/dtds/podcast-1.0.dtd',
                    'xmlns:content':'http://purl.org/rss/1.0/modules/content/',
                    'xmlns:cnettv':'http://cnettv.com/mrss/',
                    'xmlns:creativeCommons':'http://cnettv.com/mrss/',
                    'xmlns:media':'http://search.yahoo.com/mrss/',
                    'xmlns:atom':'http://www.w3.org/2005/Atom',
                    'xmlns:amp':'http://www.adobe.com/amp/1.0',
                    'xmlns:dc':'http://purl.org/dc/elements/1.1/',
                    'xmlns:mythtv':'http://www.mythtv.org/wiki/MythNetvision_Grabber_Script_Format'
                }
        ))

    channelNode = nodeRss.appendChild(domOutput.createNode('channel'))
    channelNode.appendChild(domOutput.createNode('title',__title__))
    channelNode.appendChild(domOutput.createNode('link',channel['link']))
    channelNode.appendChild(domOutput.createNode('description',channel['description']))

    #Recherche avec l'api de canal
    url = urlSearch%quote(text)
    logger.info('Recherche via "%s"'%url)
    domInputSearch = minidom.parse(urlopen(url))
    videoInputNodes = domInputSearch.getElementsByTagName('VIDEO')
    logger.info('  -> %i résultats'%len(videoInputNodes))

    for videoInputNode in videoInputNodes :
        itemOutputNode = domOutput.convertVideo2itemNode(videoInputNode)
        if itemOutputNode :
            channelNode.appendChild(itemOutputNode)

    return domOutput


def configure() :
    '''
        Affiche un interface "Dialog" permettant de séléctionner les programmes
        qui nous intéresse (l'emsemble de programmes est vraiment très long à
        charger dans mythtv ...
    '''

    from dialog import Dialog

    config = RawConfigParser()
    config.read(configFile)
    if not config.has_section('programs') :
        config.add_section('programs')
    if config.has_option('programs','subscribed') :
        logger.info('Lecture de la configuration')
        oldSubscribedPrograms = config.get('programs','subscribed').split(',')
        logger.info('  - Abonnement : %s'%','.join(oldSubscribedPrograms))
    else :
        oldSubscribedPrograms = []

    domDoc = minidom.parse(urlopen(urlCatalogThematiques))
    programs = []
    for domThematique in domDoc.getElementsByTagName('THEMATIQUE') :
        thematiqueName = domThematique.getFirstChildByTagName('NOM').firstChild.data
        logger.debug('Thematique "%s"'%thematiqueName)
        for domSelection in domThematique.getElementsByTagName('SELECTION') :
            programName = domSelection.getFirstChildByTagName('NOM').firstChild.data
            logger.debug('  - Catégorie "%s"'%thematiqueName)
            programId = domSelection.getFirstChildByTagName('ID').firstChild.data
            if programId in oldSubscribedPrograms :
                programs.append((programId,"[%s] %s"%(thematiqueName,programName),1))
            else :
                programs.append((programId,"[%s] %s"%(thematiqueName,programName),0))

    dlg = Dialog()
    dlg.setBackgroundTitle('Configuration du grabbeur canal+ pour mythnetvison')
    (btCancel,newSubscribedPrograms) = dlg.checklist(
            text="Séléctionnez vos catégorie de programes : (Attention, avec tous les programmes de séléctionner la récupération prend plus d'1/3 heure !",
            choices=programs
        )

    if btCancel == 0 :
        logger.info('Sauvegarde de la configuration')
        config.set('programs','subscribed',','.join(newSubscribedPrograms))
        config.write(open(configFile,'w'))


if __name__ == "__main__" :
    opts,args = getopt(sys.argv[1:], "hvS:Tc",["help","version","search=","treeview","configure"])
    if len(opts) == 0 :
        sys.stdout.write(__usage__)
        sys.exit(0)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            sys.stdout.write(__usage__)
            sys.exit(0)
        elif opt in ("-v", "--version"):
            sys.stdout.write(domVersion().toxml())
            sys.exit(0)
        elif opt in ("-S", "--search"):
            sys.stdout.write(domSearch(arg).toxml())
            sys.exit(0)
        elif opt in ("-T", "--treeview"):
            sys.stdout.write(domTreeview().toxml())
            sys.exit(0)
        elif opt in ("-c", "--configure"):
            configure()
            sys.exit(0)
